package com.example.epc_apps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.epc_apps.login.Login;
import com.example.epc_apps.splashscreen.ScreenItem;

import java.util.ArrayList;
import java.util.List;

public class onboarding extends AppCompatActivity {

    private ViewPager screenPager;
    IntroViewPagerAdapter introViewpagerAdapater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

        //List
        List<ScreenItem> mList = new ArrayList<>();
        mList.add(new ScreenItem("What is EPC?", "EPC (Engineer passanger Car) is comunity in Mercedes Benz Indonesia that preparing and controling car making.", R.drawable.wrench_1));
        mList.add(new ScreenItem("leave Plan", "From this apps, you can plan your leave time easier and faster.", R.drawable.calendar_1));

        //setup viewpager
        screenPager = findViewById(R.id.screen_viewpager);
        introViewpagerAdapater = new IntroViewPagerAdapter(this, mList);
        screenPager.setAdapter(introViewpagerAdapater);

        Button buttonOne = findViewById(R.id.button_continue);
        buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
            }
        });

    }
}